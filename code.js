// const match = {
//     matchId: 1,
//     sport: 'Soccer',
//     teams: {
//         teamNameOne: 'Real Madrid',
//         teamNameTwo: 'Barcelona FC'
//     },
//     getInfo() {
//         console.log(this.teams.teamNameOne + ' vs ' + this.teams.teamNameTwo);
//     }
// }

// match.getInfo();
// console.log(match.teams.teamNameOne);
// match.teams.teamNameOne = 'Arsenal FC'
// match.getInfo();
// console.log(match.teams.teamNameOne);
// console.log(match.duration);

// const obj1 = {
//     name: 'myName'
// }
// console.log(obj1.name);

// const obj2 = obj1;
// obj2.name = 'myNewName'
// console.log(obj1.name);
// console.log(obj2.name);

// const obj1 = {
//     name: 'myName'
// }
// const obj2 = {
//     name: 'myName'
// }

// function checkProperties(ob1, ob2) {
//     if(ob1.name === ob2.name) {
//         return true;
//     }
//     return false;
// }
// console.log(checkProperties(obj1, obj2));

// a = [1,2,3,4];
// const b = {
//     first: 'firstElement',
//     second: 'secondElement',
//     third: 'thirdElement'
// }

// for(let i = 0; i < a.length; i++) {
//     console.log(a[i]);
// }

// for(let element in b) {
//     console.log(`${b[element]}`)
// }

// const dayString = '2020-03-09';
// const dayObject = {today: '2020-03-09'};
// const dayArray1 = ['2020-03-09'];
// const dayArray2 = ['2020-03-09', '2020-03-10'];
// const dayDate = new Date();

// function greeting(day) {
//     console.log(`Hello, today is ${day}`);
// }

// greeting(dayString);
// greeting(dayObject);

// greeting(dayArray1);
// greeting(dayArray2);

// greeting(dayDate);

// const obj1 = {
//     name: 'John',
//     surname: 'Doe'
// }

// const obj2 = {
//     surname: 'Doe',
//     name: 'John',
//     age: 24
// }

// function compareObjects(ob1, ob2) {
//     for(let element in ob1) {
//         if(ob1[element] !== ob2[element]) {
//             return false;
//         }
//     }
//     for(let element in ob2) {
//         if(ob2[element] !== ob1[element]) {
//             return false;
//         }
//     }
//     return true;
// }

// console.log(compareObjects(obj1, obj2));

// const array = [1,2,3,4];

// for(i of array) {
//     console.log(i);
// }
// console.log(i);

// const array = [1,2,3,4];

// for(let i of array) {
//     console.log(i);
// }
// console.log(i);

// const string = 'string';
// for(const i of array) {
//     console.log(i);
// }


// const string = 'string';

// for(const i in string) {
//     console.log(string[i]);
// }
// console.log(string);

// const string = 'string';
// for(const i of string) {
//     console.log(i);
// }
// console.log(string);


// const match = {
//     matchId: 5,
//     sportId: 100,
//     sportName: 'Soccer',
//     teams: {
//         teamNameOne: 'Real Madrid',
//         teamNameTwo: 'Barcelona FC'
//     }
// }
// const matchCopy = match;
// console.log(matchCopy);
// console.log(matchCopy === match);
// console.log(JSON.stringify(matchCopy) === JSON.stringify(match));

// const match = {
//     matchId: 5,
//     sportId: 100,
//     sportName: 'Soccer',
//     teams: {
//         teamNameOne: 'Real Madrid',
//         teamNameTwo: 'Barcelona FC'
//     }
// }
// const matchCopy = {...match};
// console.log(matchCopy === match);
// console.log(JSON.stringify(matchCopy) === JSON.stringify(match));
// console.log(matchCopy);

// const match = {
//     matchId: 5,
//     sportId: 100,
//     sportName: 'Soccer',
//     teams: {
//         teamNameOne: 'Real Madrid',
//         teamNameTwo: 'Barcelona FC'
//     }
// }

// const matchCopy = {...match};
// match.teams.teamNameOne = 'Real Madrid FC'
// console.log(match);
// console.log(matchCopy === match);
// console.log(JSON.stringify(matchCopy)===JSON.stringify(match));

// const match = {
//     matchId: 5,
//     sportId: 100,
//     sportName: 'Soccer',
//         teams: {
//             teamNameOne: 'Real Madrid',
//             teamNameTwo: 'Barcelona FC'
//         }
// }

// const matchCopy1 = match;
// const matchCopy2 = {...match};
// const matchCopy3 = JSON.parse(JSON.stringify(match));   // pretvara string u objekt zato se izmjena nije dogodila jer vise se ne radi o objektu
                                                        //  na kojem se rade izmjene!!!!
// matchCopy1.sportId = 101;
// matchCopy1.sportId = 102;
// matchCopy1.sportId = 103;

// console.log(match);

// matchCopy1.teams.teamNameOne = 'Real Madrid 1';
// matchCopy2.teams.teamNameOne = 'Real Madrid 2';
// matchCopy3.teams.teamNameOne = 'Real Madrid 3';

// console.log(match);

// const array = [1,2,3,4,5,6];

// console.log(array.length);
// array.push(7);
// console.log(array);
// array.pop();
// console.log(array);
// array.pop(6);
// console.log(array);
// array[0] = 0
// console.log(array);

// const array =   
// [
//     {
//         sportName: 'Nogomet',
//         sportOrder: 4
//     },

//     {
//         sportName: 'Tenis',
//         sportOrder: 2
//     },

//     {
//         sportName: 'Odbojka',
//         sportOrder: 3
//     }
// ]

// function orderObjects(obj1, obj2) {
//     if(obj1.sportOrder < obj2.sportOrder) {
//         return -1;
//     }

//     if(obj1.sportOrder > obj2.sportOrder) {
//         return 1;
//     }

//     return 0;
// }

// function orderObjects_descending(obj1, obj2) {
//     if(obj1.sportOrder > obj2.sportOrder) {
//         return 1;
//     } if (obj1.sportOrder < obj2.sportOrder) {
//         return -1;
//     }
//     return 0;
// }

// Za sutra, ako dođeš prije mene:
// Napiši funkcije koja će kao parametar primiti sljedeće polje i:
// [
// {name: 'first',
//  value: 10,
// },
// {name: 'second',
//  value: 5,
// },
// {name: third
//  value: 18,
// },
// {name: fourth
//  value: 0,
// },
// ]
// 1. Pronaći najveći broj u polju i ispisati ga.
// 2. Sortirati polje po value-u, zatim ispisati polje.
// 3. Svakom objektu u polju pribrojati 2 na value, zatim ispisati polje.
// 4. Obrisati polje s najvećim valueom, zatim ispisati polje.
// 5. Stvoriti novo polje u kojem će se nalaziti samo objekti polja s parnim valueima.


const i = [
    {

    name: "first",
    value: 10

    },
    
    {
        name: "second",
        value: 5,
    },

    {
        name: "third",
        value: 18
    },

    {
        name: "fourth",
        value: 0
    }
]

// 1. pronaci najveći broj i ispisati ga:

console.log(Math.max(...i.map(o => o.value)));

// 2. Sortirati objekt po property value- u i ispisati ga

    function sorting(ob1, ob2) {
        if(ob1.value > ob2.value) {
            return -1;
        }
        if(ob1.value < ob2.value) {
            return 1;
        }
        return 0;
    }
console.log(i.sort(sorting));

// 3. Svakom value property pridodati vrijednost

    i.forEach(function(value){
        var val = value.value+2;
        console.log(val);
    })

// 4. Obrsiati objekt s najvecim value-om

    // function delMax(obj, ob1,ob2){
    //     let maxVal = Math.max(...obj.map(o => o.value));
    //         if(ob1.value == maxVal){
                
    //         }
    // }
    function deleteMaximum(obj) {
        let maxVal = Math.max(...obj.map(o => o.value));
        for(let i = 0; i < obj.length; i++) {
            if(obj[i].value==maxVal) {
                obj.pop();
            }
        }
        return obj;
    }
    console.log(deleteMaximum(i));

// 5. Kreirati polje samo s parnim value-ima



    function findEven(obj) {
        const newArray = [];
        for(let i = 0; i<obj.length; i++) {
            if(obj[i].value%2==0) {
                newArray.push(obj[i]);
            }
        }

        return newArray;
    }

    console.log(findEven(i));